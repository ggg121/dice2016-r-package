####################################
#
# fun_with_dice.r   20 April 2017
#
# Author: Gregory Garner
#   ggarner@princeton.edu
#
# This code is provided in the hopes
# that it is useful. The author(s)
# provide no warranty, either
# explicit or implicit, about the
# performance of this code. The
# author(s) are not responsible for
# any damages caused by the
# use of this code.
#
####################################

# For convenience, be sure to set your
# working directory to the location of
# this script. The DICE package
# "DICE_2016.1.tar.gz" should also be
# contained in this directory.

# INSTALL THE DICE PACKAGE
# Uncomment the call to install.packages()
# below to install the DICE package.
# NOTE: For Windows, please be sure
# to install Rtools appropriate to
# your R distribution from:
# https://cran.r-project.org/bin/windows/Rtools/
#-------------------------------------
#install.packages("DICE_2016.1.tar.gz", repos=NULL, type="source")

# Load the DICE library
library(DICE)

# This loads the model "dice()" and a small dataset 
# "dice.optimal" that contains the optimal control
# policy (dice.optimal$miu) and savings rate
# (dice.optimal$s).

# For illustration, let's run the model with the
# optimal solution.
dice.result <- dice(miu = dice.optimal$miu, s = dice.optimal$s)

# The result is a list of four lists:
# pars - The parameters used in this run
# dvars - The decision variables used (miu and s)
# exog - Time series variables calculated exogenously
# endog - Time series variables calculated endogenously

# For example, print the Utility from this run
dice.result$endog$utility

# ...or the time series of atmospheric temperature
dice.result$endog$tatm

# These variables are described in the help file
# for this function (?dice)

# You can change a parameter value in the model
# the same way you would change the parameter
# in any function call in R. Let's do a run
# where the climate sensitivity (t2xco2) is set
# to 6.0
dice.result2 <- dice(miu = dice.optimal$miu, s = dice.optimal$s,
                     t2xco2 = 6.0)

# ...and see what that did to the atmospheric
# temperature projection.
par(mar=c(4.5,4.5,1,1)+0.1)
plot(dice.result2$exog$dateSeries, dice.result2$endog$tatm,
     type="l", col="black", lty=2, xlab="Year", 
     ylab="Atmos. Temp. Change [degC from 1900]")
lines(dice.result2$exog$dateSeries, dice.result$endog$tatm,
      col="black", lty=1)
legend("topleft", legend=c("t2xco2 = 2.9", "t2xco2 = 6.0"),
       lty=c(1,2), col="black", bty="n")

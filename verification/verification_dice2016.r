###########################################################
#
# verification_dice2016.r       20 April 2017
#
# Generates plots to verify the DICE2016 R package
# against the results from the original GAMS code
#
###########################################################

# Directories
maindir <- "./"

# Libraries and sourced code
library(DICE)

# Plot file
plotfile <- paste(maindir, "/dice2016_verification_plots.pdf", sep="")

# Read in the verification data
verificationfile <- paste(maindir, "/Dice2016R-091916ap.csv", sep="")
dice.verification <- read.csv(verificationfile, header=F, row.names=1)

# Remove the redundant lines of data
dice.verification <- dice.verification[-c(1,3,5,30),]

# Run the dice2016 model with optimal dvars
results <- dice(dice.optimal$miu, dice.optimal$s)

# Map the R package results to the verification file
ind.1 <- c(3,4,4, 4, 4, 4, 4,2, 4, 4,3,3, 4,3, 4,2, 4, 4, 4, 4, 4,3,4,3, 4,4, 3, 4, 4,4, 4,4,4,4, 4, 4)
ind.2 <- c(1,8,2,14,19,11,25,1,30,13,2,3,16,6,10,2,12,17,18,19,20,4,1,7,24,9, 9,22,23,4,29,7,5,6,27,28)

# Loop over the variables to plot
pdf(plotfile, width = 3.5, height = 2.7, pointsize = 7)
par(mar=c(4.5,4.5,1,1)+0.1)
plot.new()
plot.window(xlim=c(0,1), ylim=c(0,1))
legend("left", legend=c("Original Gams", "R Package"), 
       pch=c(1,NA), lty=c(NA,1), bty="n", cex=2.8)
for(i in 1:length(ind.1)){
  plot.values <- results[[ind.1[i]]][[ind.2[i]]]
  if(ind.1[i] == 2 & ind.2[i] == 1){
    plot.values <- c(results$pars$miu0, plot.values)
  }
  if(ind.1[i] == 2 & ind.2[i] == 2){
    plot.values <- c(plot.values, rep(results$pars$optlrsav, 10))
  }
  plot(as.numeric(dice.verification[1,]), as.numeric(dice.verification[i,]),
       ylab=row.names(dice.verification)[i], xlab="Year")
  lines(results$exog$dateSeries, plot.values)
}
dev.off()

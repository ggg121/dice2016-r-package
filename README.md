# DICE2016R package for R #

This DICE R package is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
 
This DICE R package is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.
 
You should have received a copy of the GNU Lesser General Public License
along with the DICE R package code.  If not, see <http://www.gnu.org/licenses/>.


This directory contains the following:

* DICE_2016.1.tar.gz - The DICE2016 R package
* fun_with_dice*.r   - Example exercises that show how to use the package
* LICENSE            - GPL v.3 license for the R package and source code
                     contained therein
* README             - The document you are reading now
* verification       - Directory that contains code, data, and plots that
                     show the output of the ported code matches the original


The simplest way to install this package is to open R or RStuido, navigate to the directory containing your cloned copy of this repo, and uncomment/run line 33 in any of the 
"fun_with_dice" examples.  Note that Windows users will need to install 
Rtools since this package requires compilation of source code:

https://cran.r-project.org/bin/windows/Rtools/


The DICE model is developed and maintained by William Nordhaus at 
Yale University.  See the link below for access to his latest version:

http://www.econ.yale.edu/~nordhaus/homepage/
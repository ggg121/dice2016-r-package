####################################
#
# fun_with_dice2.r   20 April 2017
#
# Author: Gregory Garner
#   ggarner@princeton.edu
#
# This code is provided in the hopes
# that it is useful. The author(s)
# provide no warranty, either
# explicit or implicit, about the
# performance of this code. The
# author(s) are not responsible for
# any damages caused by the
# use of this code.
#
####################################

# For convenience, be sure to set your
# working directory to the location of
# this script. The DICE package
# "DICE_2016.1.tar.gz" should also be
# contained in this directory.

# INSTALL THE DICE PACKAGE
# Uncomment the call to install.packages()
# below to install the DICE package.
# NOTE: For Windows, please be sure
# to install Rtools appropriate to
# your R distribution from:
# https://cran.r-project.org/bin/windows/Rtools/
#-------------------------------------
#install.packages("DICE_2016.1.tar.gz", repos=NULL, type="source")

# Load the DICE library
library(DICE)

# How does the optimal policy change if we change
# a parameter in the model?

# Create a function that can be used in optim
# Note that we change the "dk" parameter to 0.2
opt.dice <- function(dvars) {
  result <- dice(dvars[1:99], dvars[100:189], dk=0.2)
  -1 * result$endog$utility
}

# Let's optimize! Since the upper limit of miu
# changes half-way into the simulation, it's easiest
# to create a variable and pass that to the "upper"
# parameter in optim(). Also note that we use the
# original optimal solution as a first-guess.
dvar.uplim <- c(rep(1,28), rep(1.2,71), rep(1,90))
my.opt <- optim(par = c(dice.optimal$miu, dice.optimal$s),
                fn = opt.dice,
                method = "L-BFGS-B",
                lower = rep(0,189), upper = dvar.uplim)
dice.result <- dice(my.opt$par[1:99], my.opt$par[100:189], dk=0.2)

# Make a plot of the original optimal control policy
plot(x=dice.result$exog$dateSeries, 
     y=c(dice.result$pars$miu0, dice.optimal$miu),
     xlab="Year",
     ylab="Control Policy",
     type="l",
     lty=1)

# Add the optimal control policy from the modified run
lines(x=dice.result$exog$dateSeries,
      y=c(dice.result$pars$miu0, my.opt$par[1:99]),
      lty=2)

# Put a legend at the bottom of the plot
legend("bottom", 
       legend=c("Original", "Modified"), 
       lty=c(1,2),
       bty="n")

# Done!
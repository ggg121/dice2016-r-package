####################################
#
# fun_with_dice3.r   20 April 2017
#
# Author: Gregory Garner
#   ggarner@princeton.edu
#
# This code is provided in the hopes
# that it is useful. The author(s)
# provide no warranty, either
# explicit or implicit, about the
# performance of this code. The
# author(s) are not responsible for
# any damages caused by the
# use of this code.
#
####################################

# For convenience, be sure to set your
# working directory to the location of
# this script. The DICE package
# "DICE_2016.1.tar.gz" should also be
# contained in this directory.

# INSTALL THE DICE PACKAGE
# Uncomment the call to install.packages()
# below to install the DICE package.
# NOTE: For Windows, please be sure
# to install Rtools appropriate to
# your R distribution from:
# https://cran.r-project.org/bin/windows/Rtools/
#-------------------------------------
#install.packages("DICE_2016.1.tar.gz", repos=NULL, type="source")

# Load the DICE library
library(DICE)

# Let's sample the uncertainty about a
# parameter and view its effect on
# the temperature in 2100 all while using
# the optimal control policy

# How many samples do we want?
n.samples <- 300

# Create a variable to hold the temperature
# values from all the runs
my.temperatures <- array(NA, dim=n.samples)

# Let's vary the t2xco2 parameter
my.t2xco2.vals <- rlnorm(n.samples, meanlog=log(3.1), sdlog=log(1.5))

# Loop over the number of samples...
for(i in 1:n.samples) {
  
  # Modify the parameter in dice and run the model
  dice.result <- dice(miu = dice.optimal$miu,
                      s = dice.optimal$s,
                      t2xco2 = my.t2xco2.vals[i])
  
  # Store the temperature in 2100
  year.index <- which(dice.result$exog$dateSeries == 2100)
  my.temperatures[i] <- dice.result$endog$tatm[year.index]
}

# Make a histogram of the temperatures
hist(my.temperatures, 
     breaks=20, 
     prob=T,
     main="Temperature Change in 2100", 
     xlab="Temperature change [deg C]")

# Done!